package com.reobotnet.resources;



import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.reobotnet.domain.Cliente;
import com.reobotnet.dto.ClienteDTO;
import com.reobotnet.dto.ClienteNewDTO;
import com.reobotnet.services.ClienteService;

@RestController
@RequestMapping(value="/clientes")
public class ClienteResource {
	
			
		@Autowired
		private ClienteService service;
	
		@RequestMapping(value="/{id}",method=RequestMethod.GET)
		public ResponseEntity<Cliente> find(@PathVariable Integer id){
			
			
			Cliente obj = service.find(id);
			return ResponseEntity.ok().body(obj);
			
		}
		
		
		// Insere
		@RequestMapping(method = RequestMethod.POST)
		// @ResquestBody -> converte json para java
		//@Valid valida o objeto DTO antes dele ser passado para a frente
		public ResponseEntity<Void> insert(@Valid @RequestBody ClienteNewDTO objDTO) {
			//Converte Obj DTO para Ojb Entity
			Cliente obj = service.fromDTO(objDTO);
			obj = service.insert(obj);
//			// Exibe a url + id
//			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
//			return ResponseEntity.created(uri).build();
			return ResponseEntity.noContent().build();
		}

		
		// Atualiza
		@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
		//@Valid valida o objeto DTO antes dele ser passado para a frente
		public ResponseEntity<Void> update(@Valid @RequestBody ClienteDTO objDTO, @PathVariable Integer id) {
			//Converte Obj DTO para Ojb Entity
			Cliente obj = service.fromDTO(objDTO);
			// Garante que o conteúdo a ser atualizado é o mesmo que o id passado na url
			obj.setId(id);
			obj = service.update(obj);
			// noContent retorna um conteúdo vazio
			return ResponseEntity.noContent().build();

		}

		// Excluir
		@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
		public ResponseEntity<Cliente> delete(@PathVariable Integer id) {
			service.delete(id);
			// noContent retorna um conteúdo vazio
			return ResponseEntity.noContent().build();
		}

		// Lista todas
		@RequestMapping(method = RequestMethod.GET)
		public ResponseEntity<List<ClienteDTO>> findAll() {
			List<Cliente> list = service.findAll();
			List<ClienteDTO> listDTO = list.stream().map(obj -> new ClienteDTO(obj)).collect(Collectors.toList());
			return ResponseEntity.ok().body(listDTO);
		}

		// Paginação
		@RequestMapping(value="/page",method = RequestMethod.GET)
		public ResponseEntity<Page<ClienteDTO>> findPage(
				@RequestParam(value = "page", defaultValue = "0") Integer page,
				@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage, 
				@RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
				@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
			Page<Cliente> list = service.findPage(page,linesPerPage, orderBy, direction);
			Page<ClienteDTO> listDTO = list.map(obj -> new ClienteDTO(obj));
			return ResponseEntity.ok().body(listDTO);
		}



}
