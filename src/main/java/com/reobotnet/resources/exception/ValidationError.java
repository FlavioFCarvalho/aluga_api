package com.reobotnet.resources.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidationError extends StandardError {
	private static final long serialVersionUID = 1L;
	
	private List<FieldMessage> list = new ArrayList<>();
	
	public ValidationError(Integer status, String msg, Long timeStamp) {
		super(status, msg, timeStamp);
		
	}

	public List<FieldMessage> getErrors() {
		return list;
	}

	//Trazendo um erro de cada vez e não a lista toda de erros
	public void addError(String fieldName, String messagem) {
		list.add(new FieldMessage(fieldName,messagem));
	}

	

}
