package com.reobotnet;


import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.reobotnet.domain.Categoria;
import com.reobotnet.domain.Cidade;
import com.reobotnet.domain.Cliente;
import com.reobotnet.domain.Endereco;
import com.reobotnet.domain.Estado;
import com.reobotnet.domain.ItemPedido;
import com.reobotnet.domain.Pagamento;
import com.reobotnet.domain.PagamentoComBoleto;
import com.reobotnet.domain.PagamentoComCartao;
import com.reobotnet.domain.Pedido;
import com.reobotnet.domain.Produto;
import com.reobotnet.domain.enums.EstadoPagamento;
import com.reobotnet.domain.enums.TipoCliente;
import com.reobotnet.repositories.CategoriaRepository;
import com.reobotnet.repositories.CidadeRepository;
import com.reobotnet.repositories.ClienteRepository;
import com.reobotnet.repositories.EnderecoRepository;
import com.reobotnet.repositories.EstadoRepository;
import com.reobotnet.repositories.ItemPedidoRepository;
import com.reobotnet.repositories.PagamentoRepository;
import com.reobotnet.repositories.PedidoRepository;
import com.reobotnet.repositories.ProdutoRepository;



@SpringBootApplication
public class AlugaapiApplication implements CommandLineRunner{
	
	@Autowired
	private CategoriaRepository categoriaRepository; 
	
	@Autowired
	private ProdutoRepository  produtoRepository; 
	
	@Autowired
	private CidadeRepository cidadeRepository; 
	
	@Autowired
	private EstadoRepository estadoRepository; 
	
	@Autowired
	private ClienteRepository clienteRepository; 
	
	
	@Autowired
	private EnderecoRepository enderecoRepository; 
	
	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private ItemPedidoRepository itemPedidoRepository;


	public static void main(String[] args) {
		SpringApplication.run(AlugaapiApplication.class, args);
		
		
		
	}

	@Override
	public void run(String... args) throws Exception {
		
		Categoria cat1 = new Categoria (null, "Informatica");
		Categoria cat2 = new Categoria (null, "Eletro doméstico");
		Categoria cat3 = new Categoria (null, "Cama mesa e banho");
		Categoria cat4 = new Categoria (null, "Jardinagem");
		Categoria cat5 = new Categoria (null, "Terrenos");
		Categoria cat6 = new Categoria (null, "Estradas");
		Categoria cat7 = new Categoria (null, "Vidros");
		
		
		Produto p1 = new Produto(null, "Computador", 2000.00);
		Produto p2 = new Produto(null, "Impressora", 800.00);
		Produto p3 = new Produto(null, "Computador", 80.00);
		Produto p4 = new Produto(null, "Tv 40'", 1200.00);
		Produto p5 = new Produto(null,"Toalha de messa ", 70.00);
		Produto p6 = new Produto(null,"X box one", 2000.00);
		Produto p7 = new Produto(null, "Toalha de banho", 45.00);
		Produto p8 = new Produto(null, "Smart tv ", 400.00);
		Produto p9 = new Produto(null, "Som 3 em 1", 200.00);
		Produto p10 = new Produto(null, "Roçadeira", 150.00);
		Produto p11 = new Produto(null,"Abaju", 95.00);
	
		
		cat1.getProdutos().addAll(Arrays.asList(p1,p2,p3));
		cat2.getProdutos().addAll(Arrays.asList(p2,p4,p6,p8,p9,p11));
		cat3.getProdutos().addAll(Arrays.asList(p5,p7));
		cat4.getProdutos().addAll(Arrays.asList(p10));
		
		p1.getCategorias().addAll(Arrays.asList(cat1));
		p2.getCategorias().addAll(Arrays.asList(cat1,cat2));
		p3.getCategorias().addAll(Arrays.asList(cat1));
		p4.getCategorias().addAll(Arrays.asList(cat2));
		p5.getCategorias().addAll(Arrays.asList(cat3));
		p6.getCategorias().addAll(Arrays.asList(cat2));
		p7.getCategorias().addAll(Arrays.asList(cat3));
		p8.getCategorias().addAll(Arrays.asList(cat2));
		p9.getCategorias().addAll(Arrays.asList(cat2));
		p10.getCategorias().addAll(Arrays.asList(cat4));
		p11.getCategorias().addAll(Arrays.asList(cat2)); 

		
		categoriaRepository.save(Arrays.asList(cat1, cat2, cat3, cat4, cat5, cat6, cat7));
		produtoRepository .save(Arrays.asList(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11 ));
		
		Estado est1 = new Estado (null, "Minas Gerais");
		Estado est2 = new Estado (null, "São Paulo");
		Estado est3 = new Estado (null,"Rio de Janeiro");
		
		
		Cidade c1 = new Cidade (null, "Uberlândia", est1);
		Cidade c2 = new Cidade (null, "São Paulo", est2);
		Cidade c3 = new Cidade (null, "Campinas", est2);
		Cidade c4 = new Cidade (null, "Campos dos Goytacazes",est3);
		
		est1.getCidades().addAll(Arrays.asList(c1));
		est2.getCidades().addAll(Arrays.asList(c2, c3));
		est3.getCidades().addAll(Arrays.asList(c4));
		
		estadoRepository.save(Arrays.asList(est1, est2, est3));
		cidadeRepository.save(Arrays.asList(c1,c2,c3,c4));
		
		Cliente cli1 = new Cliente(null, "Maria Silva","maria@gmail.com","222555588", TipoCliente.PESSOAFISICA);
		
		cli1.getTelefones().addAll(Arrays.asList("27235488","999998888"));
		
		Endereco e1 = new Endereco(null,"Gastão Machado", "66", "sala 205", "Centro", "28035-120", cli1, c1);
		
		Endereco e2 = new Endereco(null,"Machado de Assis", "66", "sala 205", "Centro", "28035-120", cli1, c2);
		
		
        cli1.getEnderecos().addAll(Arrays.asList(e1,e2));
        
         Cliente cli2 = new Cliente(null, "Flávio Carvalho", "flaviocfr@gmail.com", "144404005/0001-43", TipoCliente.PESSOAJURIDICA);
        
         cli2.getTelefones().addAll(Arrays.asList("2334444444", "34444444"));
         
         Endereco e3 = new Endereco(null, "João Cabral de Mello Neto", "58", "apto 202 bl 09", "Jockei", "28100000", cli2, c4);
         
         cli2.getEnderecos().addAll(Arrays.asList(e3));
         
         
         
         clienteRepository.save(Arrays.asList(cli1,cli2));
        
        enderecoRepository.save(Arrays.asList(e1, e2,e3));
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        
        Pedido ped1 = new Pedido(null, sdf.parse("20/02/2108 10:34"), cli1, e1);
        Pedido ped2 = new Pedido(null, sdf.parse("20/01/2108 10:50"), cli1, e2);
        
        
        Pagamento pagto1 = new PagamentoComCartao(null, EstadoPagamento.QUITADO, ped1, 6);
        ped1.setPagamento(pagto1);
        
       Pagamento pagto2 = new PagamentoComBoleto(null, EstadoPagamento.PENDENTE, ped2,sdf.parse( "20/01/2108 00:00"), null);
       ped2.setPagamento(pagto2);
       
       cli1.getPedidos().addAll(Arrays.asList(ped1, ped2));
       
       pedidoRepository.save(Arrays.asList(ped1,ped2));
       
       pagamentoRepository.save(Arrays.asList(pagto1,pagto2 ));
       
       ItemPedido ip1 = new ItemPedido(ped1, p1, 0.00, 1, 2000.00);
       ItemPedido ip2 = new ItemPedido(ped1, p3, 0.00, 2, 80.00);
       ItemPedido ip3 = new ItemPedido(ped2, p2, 100.00, 1, 800.00);
       
       ped1.getItens().addAll(Arrays.asList(ip1,ip2));
       ped2.getItens().addAll(Arrays.asList(ip3));
       
       p1.getItens().addAll(Arrays.asList(ip1));
       p2.getItens().addAll(Arrays.asList(ip3));
       p3.getItens().addAll(Arrays.asList(ip2));
       
       itemPedidoRepository.save(Arrays.asList(ip1,ip2,ip3));
		
	}}
	
		
	