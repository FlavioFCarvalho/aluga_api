package com.reobotnet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reobotnet.domain.Pedido;
import com.reobotnet.repositories.PedidoRepository;
import com.reobotnet.services.exceptions.ObjectNotFoundException;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository repo;
	
	public Pedido find(Integer id) {
		Pedido obj = repo.findOne(id);
		
		if(obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado ! Id: " + id +
					", Tipo " + Pedido.class.getName());
		}
		return obj;
	}


}
